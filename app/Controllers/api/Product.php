<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers\api;

use CodeIgniter\RESTful\ResourceController;
use App\Models\ProductModel;
use CodeIgniter\API\ResponseTrait;

/**
 * Description of Product
 *
 * @author Abdullah
 */
class Product extends ResourceController{
    use ResponseTrait;
    // get all product
    public function index() {
        $model = new ProductModel();
        if(isset($_GET['cat_id'])){
            $data = $model->where('category_id', $_GET['cat_id'])->findAll();
        }else{
            $data = $model->findAll();
        }
        return $this->respond($data);
    }
    
    // get single product
    public function show($id = null)
    {
        $model = new ProductModel();
        $data = $model->find($id);
        if($data){
            return $this->respond($data);
        }else{
            return $this->failNotFound('No Data Found with id '.$id);
        }
    }

}
