<?php namespace App\Models;


use CodeIgniter\Model;
/**
 * Description of ProductModel
 *
 * @author Abdullah
 */
class ProductModel extends Model{
    
    protected $table = 'products';
    protected $allowedFields = ['product_name','product_price','category_id'];

}
