<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
         $data = [
            [
               'login' => 'test',
               'password'    => password_hash(123123,PASSWORD_DEFAULT),
            ],
            [
               'login' => 'test2',
               'password'    => password_hash(123123,PASSWORD_DEFAULT),
            ]
        ];
        foreach ($data as $value) {
           $this->db->table('users')->insert($value);    
        }
    }
}
