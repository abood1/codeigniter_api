<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ProductSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
               'product_name' => 'BMW',
               'product_price'    => 20000,
               'category_id'=>1
            ],
            [
               'product_name' => 'Benz',
               'product_price'    => 40000,
               'category_id'=>1
            ],
            [
               'product_name' => 'Iphone6',
               'product_price'    => 2000,
               'category_id'=>2
            ],
            [
               'product_name' => 'Iphone7',
               'product_price'    => 3000,
               'category_id'=>2
            ],
        ];
        foreach ($data as $value) {
           $this->db->table('products')->insert($value);    
        }
    }
}
