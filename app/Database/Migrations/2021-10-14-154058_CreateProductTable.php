<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateProductTable extends Migration {

    public function up() {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'product_name' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'product_price' => [
                'type' => 'INT',
                'null' => true,
            ],
            'category_id' => [
                'type' => 'INT',
                'null' => true,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('products');
    }

    public function down() {
        //
    }

}
